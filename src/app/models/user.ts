
export interface User {
    id?: number,
    firstName: string,
    lastName: string,
    registeredAt: Date,
    email: string,
    birthday: Date,
    teamId: number
}