import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamCreateComponent } from './components/team-create/team-create.component';
import { TeamDetailsComponent } from './components/team-details/team-details.component';
import { TeamsListComponent } from './components/teams-list/teams-list.component';
import { TeamsPageComponent } from './components/teams-page/teams-page.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [TeamCreateComponent, TeamDetailsComponent, TeamsListComponent, TeamsPageComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule

  ]
})
export class TeamsModule { }
