import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit {

  constructor() { }

  title = 'Teams';

  @Input('teams') teamsList: Team[];
  @Output() itemSelected =  new EventEmitter<number>();

	ngOnInit() {
    }
    
    teamSelected(index: number){
        this.itemSelected.emit(index);
    }

}