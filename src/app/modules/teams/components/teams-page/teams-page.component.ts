import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/Team';
import { Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-teams-page',
  templateUrl: './teams-page.component.html',
  styleUrls: ['./teams-page.component.css'],
  providers: [TeamService]
})
export class TeamsPageComponent implements OnInit {

  hideCreate = true;
  title: string;
  teams: Team[] = [];
  selectedTeam: Team = {} as Team;
  selectedTeamIndex: number = -1;
  constructor(private teamService: TeamService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getTeams();
  }

  getTeams() {
    this.teams = [];
    this.teamService.getTeams().subscribe(x => {
      x.forEach(element => {
        this.teams.push(element);
      });
    }, error => {
      console.log(error);
    })
  }

  showEdit() {
    if (this.hideCreate) {
      this.selectedTeamIndex = -1;
    }
    this.selectedTeam = {
      name: '',
    } as Team;
    this.hideCreate = !this.hideCreate;
    this.title = "Create ";
  }

  save(newTeam: Team) {
    newTeam.createdAt = new Date(Date.now());
    if (this.selectedTeamIndex === -1) {
      this.teams.push(newTeam);
      this.teamService.postTeam(newTeam).subscribe(x => {
      });
    } else {
      newTeam.id = this.teams[this.selectedTeamIndex].id;
      this.teams[this.selectedTeamIndex] = newTeam;
      this.teamService.updateTeam(this.teams[this.selectedTeamIndex].id, newTeam).subscribe(x => {
      });

    }
    this.selectedTeamIndex = -1;
    this.hideCreate = true;
  }
  canDeactivate(): boolean | Observable<boolean> {

    if (!this.hideCreate) {
      return this.dialogService.confirm("Discard changes?");
    }
    else {
      return true;
    }
  }

  select(index: number) {
    let team = this.teams[index];
    this.selectedTeam = {
      name: team.name,
    } as Team;
    this.selectedTeamIndex = index;
    this.hideCreate = false;
    this.title = "Edit ";
  }

}