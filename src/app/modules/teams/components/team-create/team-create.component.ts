import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';


@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css'],
  providers: [TeamService]

})
export class TeamCreateComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() team: Team = {} as Team;
  @Output() teamChange = new EventEmitter<Team>();
  teamForm: FormGroup;

  constructor() { }


  ngOnInit() {
    this.teamForm = new FormGroup({
      'name': new FormControl(this.team.name, [
        Validators.required,
      ])
    });
    console.log(this.teamForm);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.team && this.teamForm) {
      this.teamForm.setValue(changes.team.currentValue)
    }
  }

  saveTeam() {
    let newTeam = this.teamForm.value
    newTeam.created_at = new Date(Date.now()).toJSON();
    this.teamChange.emit(newTeam);
    this.teamForm.reset();
  }

  revert() {
    this.teamForm.reset();
  }

}
