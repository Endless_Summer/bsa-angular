import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { Project } from 'src/app/models/project';
import { UserService } from 'src/app/services/user.service';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { TeamsListComponent } from '../teams-list/teams-list.component';
import { TeamsPageComponent } from '../teams-page/teams-page.component';
// import { UsersListComponent } from 'src/app/components/users/users-list/users-list.component';

@Component({
  selector: 'team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: Team;
  users: string[] = [];
  hideDetail = true;
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();

  constructor(private teamService: TeamService,
    private teamPage: TeamsPageComponent, private userService: UserService) { }

  ngOnInit() {
    // console.log(this.item);
    this.userService.getUsers().subscribe(x => {
      x.forEach(element => {
        if (element.teamId === this.item.id){
          this.users.push(`${element.firstName} ${element.lastName}`);
        }
      });
    })
  }
  ngOnChanges(changes: SimpleChanges): void {

}

  teamSelected() {

    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy(){

  }

  showDetail() {
    this.hideDetail = !this.hideDetail;
  }


}