import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/user';

@Component({
  selector: 'users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  constructor() { }

  title = 'Users';

  @Input('users') usersList: User[];
  @Output() itemSelected =  new EventEmitter<number>();

	ngOnInit() {
    }
    
    userSelected(index: number){
        this.itemSelected.emit(index);
    }

}