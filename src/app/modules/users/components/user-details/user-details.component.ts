import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { UsersPageComponent } from '../users-page/users-page.component';

@Component({
  selector: 'user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: User;
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();
  hideDetail = true;

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
  }

  userSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy() {
  }

  showDetail() {
    this.hideDetail = !this.hideDetail;
  }


}
