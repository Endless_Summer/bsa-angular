import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { DialogService } from 'src/app/services/dialog.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css'],
  providers: [UserService]
})
export class UsersPageComponent implements OnInit {

  hideCreate = true;
  title: string;
  users: User[] = [];
  selectedUser: User = {} as User;
  selectedUserIndex: number = -1;

  constructor(private userService: UserService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.users = [];
    this.userService.getUsers()
      .subscribe(x => {
        x.forEach(element => {
          this.users.push(element);
        });
      }, error => {
        console.log(error);
      })
  }

  showEdit() {
    if (this.hideCreate) {
      this.selectedUserIndex = -1;
    }
    this.selectedUser = {
      firstName: '',
      lastName: '',
      birthday: new Date(),
      email: '',
      teamId: null
    } as User;
    this.hideCreate = !this.hideCreate;
    this.title = "Create ";
  }
  canDeactivate(): boolean | Observable<boolean> {

    if (!this.hideCreate) {
      return this.dialogService.confirm("Discard changes?");
    }
    else {
      return true;
    }
  }

  save(newUser: User) {
    newUser.registeredAt = new Date(Date.now());
    if (this.selectedUserIndex === -1) {
      this.users.push(newUser);
      this.userService.postUser(newUser).subscribe(x => {
      });
    } else {
      newUser.id = this.users[this.selectedUserIndex].id;
      this.users[this.selectedUserIndex] = newUser;
      this.userService.updateUser(this.users[this.selectedUserIndex].id, newUser).subscribe(x => {
      });

    }
    this.selectedUserIndex = -1;
    this.hideCreate = true;
  }


  select(index: number) {
    let user = this.users[index];
    this.selectedUser = {
      firstName: user.firstName,
      lastName: user.lastName,
      birthday: user.birthday,
      email: user.email,
      teamId: user.teamId
    } as User;
    this.selectedUserIndex = index;
    this.hideCreate = false;
    this.title = "Edit ";
  }

}