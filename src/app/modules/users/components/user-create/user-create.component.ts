import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';


@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css'],
  providers: [UserService]

})
export class UserCreateComponent implements OnInit, OnChanges {

  @Input() title: string;
  @Input() user: User = {} as User;
  @Output() userChange = new EventEmitter<User>();
  userForm: FormGroup;
  
  constructor() { }

  ngOnInit() {
    this.userForm = new FormGroup({
      'firstName': new FormControl(this.user.firstName, [
        Validators.required
      ]),
      'lastName': new FormControl(this.user.lastName, [
        Validators.required
      ]),
      'email': new FormControl(this.user.email, [
        Validators.required,
        Validators.email
      ]),
      'birthday': new FormControl(this.user.birthday, [
        Validators.required
      ]),
      'teamId': new FormControl(this.user.teamId, [
      ])
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.user && this.userForm) {
      this.userForm.setValue(changes.user.currentValue)
    }
  }

  saveUser() {
    let newUser: User = this.userForm.value
    newUser.registeredAt = new Date(Date.now());
    newUser.teamId = parseInt(this.userForm.value.teamId);
    this.userChange.emit(newUser);
    this.userForm.reset();
  }

  revert() {
    this.userForm.reset();
  }

}
