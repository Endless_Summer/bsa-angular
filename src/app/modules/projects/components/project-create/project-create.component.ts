import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';
import { DialogService } from 'src/app/services/dialog.service';
import { ProjectsPageComponent } from '../projects-page/projects-page.component';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css'],
  providers: [ProjectService]

})
export class ProjectCreateComponent implements OnInit, OnChanges {
  
  @Input() title: string;
  @Input() project: Project = {} as Project;
  @Output() projectChange = new EventEmitter<Project>();
  userIds: number[];
  projectForm: FormGroup;
  

  constructor() { }


  ngOnInit() {
    this.projectForm = new FormGroup({
      'name': new FormControl(this.project.name, [
          Validators.required,
      ]),
      'description': new FormControl(this.project.description, [
        Validators.required,
        Validators.maxLength(250)
      ]),
      'deadline': new FormControl(this.project.deadline,                [
          Validators.required,
      ]),
      'teamId': new FormControl(this.project.teamId, [
        Validators.required
      ]),
      'authorId': new FormControl(this.project.authorId,[
          Validators.required
      ])
  });
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes.project && this.projectForm){
        this.projectForm.setValue(changes.project.currentValue)
    }
  }

  saveProject() {
    let newProject : Project = this.projectForm.value
    newProject.createdAt = new Date(Date.now());
    newProject.authorId = parseInt(this.projectForm.value.authorId);
    newProject.teamId = parseInt(this.projectForm.value.teamId);
    this.projectChange.emit(newProject);
    this.projectForm.reset();
}

  revert(){
    this.projectForm.reset();
  }

}
