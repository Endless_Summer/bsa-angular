import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges, LOCALE_ID} from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { ProjectsPageComponent } from '../projects-page/projects-page.component';

@Component({
  selector: 'project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'],
  providers: [ProjectService]
})
export class ProjectDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: Project;
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();
  hideDetail = true;

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
  }

  projectSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy(){
  }
  updateProject() {
  }

  showDetail() {
    this.hideDetail = !this.hideDetail;
  }


}
