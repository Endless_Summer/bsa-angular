import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css'],
  providers: [ProjectService]
})
export class ProjectsPageComponent implements OnInit {

  hideCreate = true;
  title: string;
  projects: Project[] = [];
  selectedProject: Project = {} as Project;
  selectedProjectIndex: number = -1;

  constructor(private projectService: ProjectService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projects = [];
    this.projectService.getProjects().subscribe(x => {
      x.forEach(element => {
        this.projects.push(element);
      });
    }, error => {
      console.log(error);
    })
  }

  showEdit() {
    if (this.hideCreate) {
      this.selectedProjectIndex = -1;
    }
    this.selectedProject = {
      name: '',
      description: '',
      deadline: new Date(),
      teamId: null,
      authorId: null
    } as Project;
    this.hideCreate = !this.hideCreate;
    this.title = "Create ";
  }

  save(newProject: Project) {
    newProject.createdAt = new Date(Date.now());
    if (this.selectedProjectIndex === -1) {
      this.projects.push(newProject);
      this.projectService.postProject(newProject).subscribe(x => {
        console.log(x);
      });
    } else {
      newProject.id = this.projects[this.selectedProjectIndex].id;
      this.projects[this.selectedProjectIndex] = newProject;
      this.projectService.updateProject(this.projects[this.selectedProjectIndex].id, newProject).subscribe(x => {
        console.log(x);
      });
    }
    this.selectedProjectIndex = -1;
    this.hideCreate = true;

  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.hideCreate) {
      return this.dialogService.confirm("Discard changes?");
    }
    else {
      return true;
    }
  }

  select(index: number) {
    let project = this.projects[index];
    this.selectedProject = {
      name: project.name,
      description: project.description,
      deadline: project.deadline,
      authorId: project.authorId,
      teamId: project.teamId
    } as Project;
    this.selectedProjectIndex = index;
    this.hideCreate = false;
    this.title = "Edit ";
  }


}
