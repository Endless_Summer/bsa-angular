import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/task';
import { Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-tasks-page',
  templateUrl: './tasks-page.component.html',
  styleUrls: ['./tasks-page.component.css'],
  providers: [TaskService]
})
export class TasksPageComponent implements OnInit {

  hideCreate = true;
  title: string;
  tasks: Task[] = [];
  selectedTask: Task = {} as Task;
  selectedTaskIndex: number = -1;
  constructor(private taskService: TaskService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this.tasks = [];
    this.taskService.getTasks()
      .subscribe(x => {
        x.forEach(element => {
          this.tasks.push(element);
        });
      }, error => {
        console.log(error);
      })
  }

  showEdit() {
    if (this.hideCreate) {
      this.selectedTaskIndex = -1;
    }
    this.selectedTask = {
      name: '',
      description: '',
      state: -1,
      projectId: null,
      performerId: null
    } as Task;
    this.hideCreate = !this.hideCreate;
    this.title = "Create ";
  }

  canDeactivate(): boolean | Observable<boolean> {

    if (!this.hideCreate) {
      return this.dialogService.confirm("Discard changes?");
    }
    else {
      return true;
    }
  }

  save(newTask: Task) {
    newTask.createdAt = new Date(Date.now());
    if (newTask.state === 2) {
      newTask.finishedAt = new Date(Date.now());
    }
    if (this.selectedTaskIndex === -1) {
      this.tasks.push(newTask);
      this.taskService.postTask(newTask).subscribe(x => {
      });
    } else {
      newTask.id = this.tasks[this.selectedTaskIndex].id;
      this.tasks[this.selectedTaskIndex] = newTask;
      this.taskService.updateTask(this.tasks[this.selectedTaskIndex].id, newTask).subscribe(x => {
      });
    }
    this.selectedTaskIndex = -1;
    this.hideCreate = true;
  }

  select(index: number) {
    let task = this.tasks[index];
    this.selectedTask = {
      name: task.name,
      description: task.description,
      state: task.state,
      projectId: task.projectId,
      performerId: task.performerId

    } as Task;
    this.selectedTaskIndex = index;
    this.hideCreate = false;
    this.title = "Edit ";
  }

}
