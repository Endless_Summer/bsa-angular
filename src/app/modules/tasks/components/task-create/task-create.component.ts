import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/task';
import { TasksPageComponent } from '../tasks-page/tasks-page.component';
import { DialogService } from 'src/app/services/dialog.service';


@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css'],
  providers: [TaskService]

})
export class TaskCreateComponent implements OnInit, OnChanges {

  @Input() title: string;
  @Input() task: Task = {} as Task;
  @Output() taskChange = new EventEmitter<Task>();
  taskForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.taskForm = new FormGroup({
      'name': new FormControl(this.task.name, [
        Validators.required,
      ]),
      'description': new FormControl(this.task.description, [
        Validators.required,
        Validators.maxLength(250)
      ]),
      'state': new FormControl(this.task.state, [
        Validators.required,
      ]),
      'projectId': new FormControl(this.task.projectId, [
        Validators.required
      ]),
      'performerId': new FormControl(this.task.performerId, [
        Validators.required
      ])
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.task && this.taskForm) {
      this.taskForm.setValue(changes.task.currentValue)
    }
  }

  saveTask() {
    let newTask: Task = this.taskForm.value;
    newTask.createdAt = new Date(Date.now());
    if (newTask.state === 2) {
      newTask.finishedAt = new Date(Date.now());
    }
    newTask.performerId = parseInt(this.taskForm.value.performerId);
    newTask.projectId = parseInt(this.taskForm.value.projectId);
    this.taskChange.emit(newTask);
    this.taskForm.reset();
  }

  revert() {
    this.taskForm.reset();
  }

}
