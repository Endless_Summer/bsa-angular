import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Project } from 'src/app/models/project';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';
import { TasksPageComponent } from '../tasks-page/tasks-page.component';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: Task;
  project: Project;
  colorState: string;
  hideDetail = true;
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();

  constructor(private projectService: ProjectService) {
  }

  ngOnInit() {
    this.projectService.getProjects().subscribe(x => {
      x.forEach(element => {
        if (element.id === this.item.projectId) {
          this.project = element;
        }
      });
    })
    switch (this.item.state) {
      case 1:
        this.colorState = 'blue';
        break;
      case 2:
        this.colorState = 'green';
        break;
      case 3:
        this.colorState = 'red';
        break;
      case 4:
        this.colorState = 'coral';
        break;
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
  }

  taskSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy() {
  }

  showDetail() {
    this.hideDetail = !this.hideDetail;
  }

}
