import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

  constructor() { }

  title = 'Tasks';

  @Input('tasks') tasksList: Task[];
  @Output() itemSelected =  new EventEmitter<number>();

	ngOnInit() {
    }
    
    taskSelected(index: number){
        this.itemSelected.emit(index);
    }

}
