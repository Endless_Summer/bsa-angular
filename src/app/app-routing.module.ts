import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from './guards/can-leave-form.guard';
import { UsersPageComponent } from './modules/users/components/users-page/users-page.component';
import { ProjectsPageComponent } from './modules/projects/components/projects-page/projects-page.component';
import { TeamsPageComponent } from './modules/teams/components/teams-page/teams-page.component';
import { TasksPageComponent } from './modules/tasks/components/tasks-page/tasks-page.component';

const routes: Routes = [
  { path: "projects", component: ProjectsPageComponent, canDeactivate: [CanDeactivateGuard] },
  { path: "tasks", component: TasksPageComponent, canDeactivate: [CanDeactivateGuard] },
  { path: "teams", component: TeamsPageComponent, canDeactivate: [CanDeactivateGuard] },
  { path: "users", component: UsersPageComponent, canDeactivate: [CanDeactivateGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})
export class AppRoutingModule { }
