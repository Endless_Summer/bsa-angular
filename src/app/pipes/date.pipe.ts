import { Pipe, PipeTransform, Component } from '@angular/core';
import { DatePipe as AngularDatePipe } from '@angular/common';
import { strict } from 'assert';

@Component({
})

@Pipe({
  name: 'datePipe'
})
export class DatePipe implements PipeTransform {
  transform(value: string): string {
    let datePipe = new AngularDatePipe('ua');

    return datePipe.transform(value, 'longDate');
  }
  
  // Или без AngularDatePipe:
  // transform(value: string): string {
  //   const uaMonths =
  //     [
  //       'січня', 'лютого', 'березня', 'квітня', 'травня', 'червня',
  //       'липня', 'серпня', 'вересня', 'жовтня', 'листопада', 'грудня'
  //     ];
  //   const date: Date = new Date(value);
  //   const day: number = date.getDate();
  //   const year: number = date.getFullYear();
  //   const month: number = date.getMonth();
  //   const uaMonth: string = uaMonths[month];
  //   const uaDate: string = `${day.toString()} ${uaMonth} ${year.toString()}`;
  //   return uaDate;
  // }

}
