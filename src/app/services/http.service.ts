import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private baseUrl: string = environment.apiUrl;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  // public headers = new HttpHeaders();

  public getHeaders(): HttpHeaders {
    return this.headers;
  }


  constructor(private http: HttpClient) { }

  getRequest<T>(url: string): Observable<T> {
    return this.http.get<T>(this.buildUrl(url));
  }

  postRequest<T>(url: string, body: any): Observable<T> {
    return this.http.post<T>(this.buildUrl(url), body, { headers: this.getHeaders() });
  }

  putRequest<T>(url: string, body: any): Observable<T> {
    return this.http.put<T>(this.buildUrl(url), body, { headers: this.getHeaders() });

  }

  public buildUrl(url: string): string {
    if (url.startsWith('http://') || url.startsWith('https://')) {
      return url;
    }
    return this.baseUrl + url;
  }
}
