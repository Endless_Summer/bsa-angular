import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {Project} from '../models/project';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
 })
export class ProjectService {
  private routePrefix = '/api/Projects';
  constructor(private httpService: HttpService) { }

  getProjects(): Observable<Project[]> {
    return this.httpService.getRequest<Project[]>(this.routePrefix);
  }

  postProject(project: Project): Observable<Project> {
    return this.httpService.postRequest<Project>(this.routePrefix, project);
  }
  
  updateProject(id: number, project: Project): Observable<Project> {
    return this.httpService.putRequest<Project>(this.routePrefix, project);
  }
  
}
