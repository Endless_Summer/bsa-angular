import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Task } from '../models/task';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private routePrefix = '/api/Tasks';
  constructor(private httpService: HttpService) { }

  getTasks(): Observable<Task[]> {
    return this.httpService.getRequest<Task[]>(this.routePrefix);
  }

  postTask(task: Task): Observable<Task> {
    return this.httpService.postRequest<Task>(this.routePrefix, task);
  }

  updateTask(id: number, task: Task) {
    return this.httpService.putRequest<Task>(this.routePrefix, task);
  }

}
