import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from '../models/team';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private routePrefix = '/api/Teams';
  constructor(private httpService: HttpService) { }

  getTeams(): Observable<Team[]> {
    return this.httpService.getRequest<Team[]>(this.routePrefix);
  }

  postTeam(team: Team): Observable<Team> {
    return this.httpService.postRequest<Team>(this.routePrefix, team);
  }

  updateTeam(id: number, team: Team) {
    return this.httpService.putRequest<Team>(this.routePrefix, team);
  }
}