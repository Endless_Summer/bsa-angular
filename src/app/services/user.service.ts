import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private routePrefix = '/api/Users';
  constructor(private httpService: HttpService) { }

  getUsers(): Observable<User[]> {
    return this.httpService.getRequest<User[]>(this.routePrefix);
  }

  postUser(user: User): Observable<User> {
    return this.httpService.postRequest<User>(this.routePrefix, user);
  }

  updateUser(id: number, user: User) {
    return this.httpService.putRequest<User>(this.routePrefix, user);
  }

}