import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuTopComponent } from './components/menu-top/menu-top.component';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeUA from '@angular/common/locales/uk';
import { StateDirective } from './directives/state.directive';
import { CanDeactivateGuard } from './guards/can-leave-form.guard';
import { UsersModule } from './modules/users/users.module';
import { TeamsModule } from './modules/teams/teams.module';
import { TasksModule } from './modules/tasks/tasks.module';
import { ProjectsModule } from './modules/projects/projects.module';

registerLocaleData(localeUA, 'ua');

@NgModule({
  declarations: [
    AppComponent,
    MenuTopComponent,
    StateDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  	AppRoutingModule,
    UsersModule,
    TeamsModule,
    TasksModule,
    ProjectsModule
  ],
  providers: [CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
