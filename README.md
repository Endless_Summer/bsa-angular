# BSAAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


Projects - POST is false

## ====================================

ng g m modules/projects

ng generate component modules/users/components/user-create --module=modules/users/users.module.ts
ng generate component modules/users/components/user-details --module=modules/users/users.module.ts
ng generate component modules/users/components/users-list --module=modules/users/users.module.ts
ng generate component modules/users/components/users-page --module=modules/users/users.module.ts


ng generate component modules/teams/components/team-create --module=modules/teams/teams.module.ts
ng generate component modules/teams/components/team-details --module=modules/teams/teams.module.ts
ng generate component modules/teams/components/teams-list --module=modules/teams/teams.module.ts
ng generate component modules/teams/components/teams-page --module=modules/teams/teams.module.ts


ng generate component modules/tasks/components/task-create --module=modules/tasks/tasks.module.ts
ng generate component modules/tasks/components/task-details --module=modules/tasks/tasks.module.ts
ng generate component modules/tasks/components/tasks-list --module=modules/tasks/tasks.module.ts
ng generate component modules/tasks/components/tasks-page --module=modules/tasks/tasks.module.ts


project

ng generate component modules/projects/components/project-create --module=modules/projects/projects.module.ts
ng generate component modules/projects/components/project-details --module=modules/projects/projects.module.ts
ng generate component modules/projects/components/projects-list --module=modules/projects/projects.module.ts
ng generate component modules/projects/components/projects-page --module=modules/projects/projects.module.ts


